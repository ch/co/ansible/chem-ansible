# Chemistry ansible-core package for Ubuntu

Provides ansible-core 2.11.3 under Python 3 together with an environment module to faciliate use.

Must be used in conjunction with an ansible-collections package.

Note: the pypi package called ansible actually provides ansible-collections, while ansible-core provides ansible-core!

Despite upstream documentation, the ansible package on pypi does NOT include ansible-core, it simply requires the ansible-core package.
The ansible package has a version number that specifies the contents of the eollections (which does not itself have an overall version number) and the required version of ansible-core that should accompany it.

Installs under `/usr/local/ansible/2.11.3/{bin,lib/python3/site-packages/ansible}`.

Environment module is provided as `/etc/environment-modules/modules/ansible/2.11.3`.

Load with `module load ansible/2.11.3`.

# Packaging instructions

0. Ensure you have all the requirements:

    * Check `requirements.txt` from the upstream ansible-core package.
    * For building the documentation/manpages, also check that you have the requirements listed in `docs/docsite/requirements.txt` from that same package.

1. Clone this repository. Hereafter, the location is assumed to be `~/work/ubuntu-packages/ansible-core`.

2. Delete contents of `ROOT/usr`.

3a. Update `ROOT/DEBIAN/control`, `ROOT/DEBIAN/postinst`, `ROOT/DEBIAN/prerm`, `ROOT/DEBIAN/conffiles.exclude` as appropriate.

3b. Update `ROOT/etc/environment-modules/modules/ansible/` as appropriate.

4. Unpack new version of `ansible-core` in another place.

    E.g. download the `.tar.gz` file from the [upstream releases page](https://github.com/ansible/ansible/releases).

5. "Install" into the package root, by running (from that other place) the following:

    ```
    python3 setup.py install --root=~/work/ubuntu-packages/chem-ansible-core/ROOT/ --prefix=/usr/local/ansible/2.11.3  --no-compile
    mv ~/work/ubuntu-packages/chem-ansible-core/ROOT/usr/local/ansible/2.11.3/lib/python3.6 ~/work/ubuntu-packages/chem-ansible-core/ROOT/usr/local/ansible/2.11.3/lib/python3
    ```
    <!-- not --install-layout=deb; we want it in site-packages this time since installing under /usr/local/ansible, not /usr -->

    <!--
    Remove all the __pycache__ directories, since we do the compiling as part of postinst instead:
    ```
    cd ~/work/ubuntu-packages/chem-ansible-core/ROOT/
    find . -name __pycache__ -exec rm -rf '{}' +
    -->

6. Build the man pages and copy them into place.

    ```
    make docs
    mkdir ~/work/ubuntu-packages/chem-ansible-core/ROOT/usr/local/ansible/2.11.3/docs
    cp -R docs/man ~/work/ubuntu-packages/chem-ansible-core/ROOT/usr/local/ansible/2.11.3/docs/
    ```


6. `make build`

7. Test, then `make upload` and `make gittag`.


## Testing
* Consider using/updating the docker images.

* Run syntax checks against the server and workstation playbooks.

* Run in check mode against a server and a workstation.

* Run in conjunction with ansible-lint.

* Consider the package build successful if the above work as you might expect (i.e. if it reports problems in our plays, that's possibly fine for now).

* Before deploying the new package, worry about any actual syntax errors, warnings or differences in the plays...

* Read output from the tests in conjunction with the relevant ansible changelogs.
  
  E.g. for the 2.11 series, this is at <https://github.com/ansible/ansible/blob/stable-2.11/changelogs/CHANGELOG-v2.11.rst>

